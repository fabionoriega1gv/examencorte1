function coctel() {
    const cocktailName = document.getElementById('nombreCoctel').value;
    const apiUrl = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${cocktailName}`;

    axios.get(apiUrl)
        .then(response => {
            const cocktail = response.data.drinks[0];
            if (cocktail) {
                document.getElementById('strCategory').innerText = `Category: ${cocktail.strCategory}`;
                const instructionsList = document.getElementById('strInstructions');
                const instructions = cocktail.strInstructions.split('\n');
                instructionsList.innerHTML = '';
                instructions.forEach(instruction => {
                    const listItem = document.createElement('li');
                    listItem.innerText = instruction;
                    instructionsList.appendChild(listItem);
                });
                document.getElementById('strDrinkThumb').src = cocktail.strDrinkThumb;
                document.getElementById('cocktail').style.display = 'block';
            } else {
                alert('Cocktail not found');
            }
        })
        .catch(error => {
            console.error('Error fetching cocktail:', error);
            alert('No se encontro el coctel');
        });
}

function limpiar() {
    document.getElementById('nombreCoctel').value = '';
    document.getElementById('strCategory').innerText = '';
    document.getElementById('strInstructions').innerHTML = '';
    document.getElementById('strDrinkThumb').src = '';
    document.getElementById('cocktail').style.display = 'none';
}